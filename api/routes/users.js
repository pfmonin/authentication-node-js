const express = require('express'); 
const router =express.Router();
const mongoose = require('mongoose'); 
const User = require('../models/user'); 
const auth = require('../middleware/auth');

const bcrypt = require('bcryptjs'); 
const jwt = require('jsonwebtoken');
const { request, json } = require('express');
const { findOne } = require('../models/user');

// login
router.post('/login', async(req, res) => {
    try {

        const { username, email, password } = req.body

        if( !username || !email || !password)
            return res.status(400).json({message: " Tous les champs doivent être remplis !"});
        
        const user = await User.findOne({email : email});
        if(!user)
            return res.status(400).json({message: "Aucun compte n'a été trouvé !"});
        
        const isMatched = await bcrypt.compare(password, user.password);
        if(!isMatched)
            return res.status(400).json({message: 'Informations incorrectes'});
        
        const token = jwt.sign({id: user._id }, process.env.JWT_SECRET);
        res.json({
            token, 
            user : {
                id : user._id,                
                username : user.username
            },
        });
        
    } catch (error) {
        res.status(400).json(error.message);
    }
}); 

// register
router.post('/register', async(req, res) => {
    try {

        const { username, email, password, passwordCheck } = req.body

        if( !username || !email || !password || !passwordCheck)
            return res.status(400).json({message: 'Tous les champs doivent être remplis'})

        if(password.length < 8)
            return res.status(400).json({message:'Votre mot de passe doit être de minimum 8 Caractères'})
        
        if(password != passwordCheck)
            return res.status(400).json({message: 'Vos mot de passe ne sont pas identiqueq'})

        const existingUser = await User.findOne({email : email })
        if(existingUser)
            return res.status(400).json({message: 'Cet utilisateur existe deja !'})
        
        const salt = await bcrypt.genSalt();
        const passHashed = await bcrypt.hash(password, salt);

        const newUser = new User ({
            username,
            email, 
            password : passHashed        
        });
        const savedUser = await newUser.save();
        res.status(200).json(savedUser);
        
    } catch (error) {
        res.status(400).json(error)
    }
});

// Get all 
router.get('/all-users', async(req, res) => {
    try {

        items = await User.find({})
        res.status(200).json(items)

    } catch (error) {
        res.status(400).json(error)
    }
});

// get one logged in 
router.get('/', auth, async(req, res) => {
    const user = await User.findById(req.user);
    res.json({
        username: user.username,
        id: user._id,
    });    
});

// update one 
router.patch('', async(req, res) => {
    try {
        
    } catch (error) {
        res.status(500).json(error)
    }
});

// delete one
router.delete('/delete', auth, async(req, res) => {
    try {

        const deletedUser = await User.findByIdAndDelete(req.user);
        res.json(deletedUser).send('Utilisateur supprimé avec succès');
        
    } catch (error) {
        res.status(500).json({error: error.message});
    }
});

// Check if the token is valid ( to know if the token is still valid after time)
router.post('/tokenIsValid', async (req, res) => {
    try {
        
        const token = req.header('x-auth-token');
        if(!token) 
            return res.json(false);
        
        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if(!verified)
            return res.json(false);
        
        const user = await User.findById(verified.id);
        if(!user)
            return json(false);

        return res.json(true);

    } catch (error) {
        res.status(500).json({error : error.message});
    }
});

module.exports = router;





