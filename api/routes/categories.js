const express = require('express'); 
const router =express.Router();
const mongoose = require('mongoose'); 
const Category = require('../models/category'); 
const auth = require('../middleware/auth');


// Get all cateogries
router.get('/all-categories', async(req, res) => {
    try {
        items = await Category.find({});
        res.status(200).json(items); 
    } catch (error) {
        res.status(400).json({message: 'Aucune categorie trouvée'});
    }
});

// Get one category
router.get('/:id', async(req, res) => {
    id = req.params.id;
    try {
        const item = await Category.findById(id);
        res.status(200).json(item);          
    } catch (error) {
        res.status(500).json({message: message.error});
    }
});    


// Post a category
router.post('/create-category', async(req, res) => {
    const name = req.body.name; 

    try {
        if(!name)
        return res.status(400).json({message: "Le nom de la catégorie doit être renseigné !"});    
    
        const newCategory = new Category ({
           name     
        });        
        const savedCategory = await newCategory.save();
        res.status(200).json(savedCategory);
        
    } catch (error) {
        res.status(500).json({error: error.message}); 
    }
    
});
module.exports = router;





