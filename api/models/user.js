const { string } = require('joi');
const mongoose = require('mongoose'); 

const userSchema = mongoose.Schema({
    username: {
        type: String, 
        minlength: 5, 
        required: true
    }, 
    email: {
        type: String, 
        required: true, 
        unique: true
    }, 
    password: {
        type: String, 
        required: true, 
        minlength: 8, 
        maxlength: 2048,        
    }, 
    createdAt: {
        type: Date,
        default: Date.now
    }, 
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('user', userSchema);
