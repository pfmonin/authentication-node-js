const mongoose = require('mongoose'); 

const RecipeSchema = mongoose.Schema({
    title : {
        type : String
    },
    subtitle : {
        type : String
    },
    description: {
        type: String,
    },
    cookingTime : {
        type: Number,
    }, 
    userId: {
        type: String, 
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }, 
    updatedAt: {
        type: Date,
        default: Date.now
    }   
});

module.exports = mongoose.model('recipe', RecipeSchema);