const { json } = require('express');
const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
   
   try {
    const token = req.header("x-auth-token");
    if(!token)
        return res.status(401).json({message: "Pas de suppression sans token d'authorisation"});

    const verified = jwt.verify(token, process.env.JWT_SECRET);
    if(!verified)
        return res.status(400).json({message: "Votre authenticité n'a pas pu être verifiée !"});

    req.user = verified.id;
    next();
   } catch (error) {
       res.status(500).json({error : error.message});
   }
};

module.exports = auth; 