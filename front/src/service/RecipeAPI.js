import Axios from 'axios'

function findAll () {
    return Axios
    .get("http://localhost:8080/recipes/")
    .then(response =>response.data)
}


function findAllFromId () {
    return Axios
    .get("http://localhost:8080/recipes/my-recipies")
    .then(response =>console.log(response))
}
export default {
    findAllFromId,
    findAll
    // delete : deleteCustomer 
}