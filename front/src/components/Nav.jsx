import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// assets
import '../assets/css/style.css'
import logo from '../assets/image/node.png'
import AuthOptions from './auth/AuthOptions';



// Logique
const Navigation = () => {   

    return(

        <>

    <Navbar variant="dark">       
        <Navbar.Brand href="#home">
            <img src={logo} className="logo" alt="logo-node" />
        </Navbar.Brand>
        <Nav className="mr-auto">
            <Link className="nav-link" to='/'>Home</Link>            
            <Link className="nav-link" to="/all-recipes">All</Link>
            <Link className="nav-link" to='/food-porn'>Food Porn</Link>
            <Link className="nav-link" to='/french'>Live Stream</Link>
            <Link className="nav-link" to='/italien'>More</Link>
        </Nav> 
                
        <AuthOptions />            
        
    </Navbar>

        </>

    );

};
export default Navigation;