import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import { UnsplashImages } from '../UnsplashImages'
import UnsplashAPI from '../../service/UnsplashAPI';


const FoodPorn = () => {
    const [ images, setImages] = useState([]);

    
    const fetchImg = async () => {
        try {
            const data = await UnsplashAPI.FindFoodImg();
            setImages(data);
            console.log(data)
            
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
        fetchImg()
    }, []);



    return (

        <>
        <div className="food-porn-page">
            <h1>Random Food image Gallery</h1>
            <div className="container food-porn mt-5 " >
                <div className="row">
                    {images.map(image => (                
                    <div className="col-md-3" key={image.id}>
                        <div className="img-container">
                            <img src={image.urls.thumb} alt="foo-img"/>                            
                        </div>     
                        <div className="content-photos">
                            <h3>{image.user.username}</h3>
                            <p>{image.alt_description}</p>
                            <a href={image.links.html}>Voir l'originale</a>
                        </div>
                    </div>
                    ))}
                </div>
            </div>
        </div>
        </>
    );
}
export default FoodPorn;