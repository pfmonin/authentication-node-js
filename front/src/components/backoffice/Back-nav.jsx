import React from 'react'; 
import { Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import '../../assets/css/style-back.css'
import arrow from '../../assets/image/icons/arrows.png'
import chef from '../../assets/image/icons/chef.png'
import beat from '../../assets/image/icons/beat.png'
import photo from '../../assets/image/icons/photo.png'
import ghost from '../../assets/image/icons/ghost.png'

// components



const backNavigation = () => {

    return (

        <> 
       <Nav className="bknav-container">          
           <div className="bkNav-nav-items-container">
               <ul>
                   <div className="close-nav"><img src={arrow} alt="back"/></div>
                   <div className="avatar">
                        <div className="bkNav-user-avatar"><img src={ghost} alt="avatar" /></div>
                   </div>

                   <Link to='/account/my-recipes' className="bkNav-nav-link" > <img src={chef} alt="cooking" /> <span className="menu-title">My Recipes</span></Link>
                   <Link to='/account/create-recipe' className="bkNav-nav-link" > <img src={beat} alt="create" /> <span className="menu-title">Create recipe</span></Link>
                   <Link to='/account/photos' className="bkNav-nav-link" ><img src={photo} alt="memories" /><span className="menu-title">Food memories</span></Link>
               </ul>
           </div>
       </Nav>

        </>

    );
}
export default backNavigation;
