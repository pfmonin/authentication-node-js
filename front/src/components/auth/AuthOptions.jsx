import React, {useContext} from 'react'
import { useHistory } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import { Nav } from 'react-bootstrap';

const AuthOptions = () => {

    const {userData, setUserData} = useContext(UserContext);


    const history = useHistory();
    const register = () => history.push('/register');
    const login = () => history.push('/login');
    const account = () => history.push('/account');
    const logout = () => {
        setUserData({
            token: undefined, 
            user: undefined,
        });
        localStorage.setItem("auth-token", "");
    };

    return (

        <Nav className="auth-button-container">
        
        {userData.user ? (
            <>
            <button className="authButton" onClick={account}>Account</button>
            <button className="authButton" onClick={logout}>Logout</button>
            </>
            ) : (

                <>
                <button className="authButton" onClick={register}>Register</button>
                <button className="authButton" onClick={login}>Login</button>
                </>
            )
        }

        </Nav> 

        
        
    );
};
export default AuthOptions;